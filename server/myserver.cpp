#include "myserver.h"

#include "item_controller_impl.h"

MyServer :: MyServer() : acceptor(ioc,
	boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 80))
{
	_server = std::make_shared < orderU::orderu_server > ();
	init();
	_server->load_stored_data();
	icm = new item_controller_impl(_server);
	for (int i = 0; i < num_workers; ++i)
	{
		workers.emplace_back(acceptor, "htdocs/");
		workers.back().start();
	}
	
}

void MyServer::init()
{
	_server->getItemManager()->onAdd.push_back([&](std::shared_ptr<item>& i) {
		});


	_server->getTableManager()->onAdd.push_back([&](std::shared_ptr<table>& i) {
		});

	_server->getTableManager()->onRemove.push_back([&](std::shared_ptr<table>& i) {

		});
	_server->getBillManager()->onAdd.push_back([&](std::shared_ptr<bill>& b) {
		});
}

void MyServer::run()
{
	_server->run();
	ioc.run();
}