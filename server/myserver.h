#pragma once

#ifndef __MYSERVER_H__
#define __MYSERVER_H__

#include <memory>

#include "http/src/http_server.h"
#include "orderu_server.h"
#include "item_controller_impl.h"

using namespace orderU;

class MyServer
{
public:
	std::shared_ptr<orderu_server> _server;

	MyServer();

	void init();
	void run();

	int num_workers = 100;

	net::io_context ioc{ 1 };
	boost::asio::ip::tcp::acceptor acceptor;

	std::list<http_worker> workers;
	item_controller_impl* icm;

};
#endif