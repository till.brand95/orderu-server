#pragma once

#ifndef __I_C_I_H__
#define __I_C_I_H__

#include <memory>
#include <functional>

#include "object/item.h"
#include "object/waiter.h"
#include "object/table.h"
#include "object/order.h"
#include "object/bill.h"
#include "mvc/controller.h"
#include "mvc/model.h"
#include "mvc/login_controller.h"
#include "mvc/user_repository.h"
#include "orderu_server.h"

using namespace orderU;

class ported_waiter : public http_server::User, public orderU::waiter
{
	std::string profile_image = "/static/img/me.png";
public:
	ported_waiter(orderU::waiter& itm) : waiter(itm), User(itm.getName(), std::to_string(itm.getCode()))
	{
		addAttribute("image", &profile_image);
	}

	ported_waiter(ported_waiter& itm) : waiter(itm), User(itm.getName(), std::to_string(itm.getCode()))
	{
		addAttribute("image", &profile_image);
	}

};

class ported_item : public http_server::model, public orderU::item
{
public:
	ported_item(orderU::item &itm) : item(itm)
	{
		addAttribute("price", &_price);
		addAttribute("name", &name);
		addFunction("getName", std::function<std::string()>(std::bind(&orderU::item::getName, this)));
	}

	ported_item(ported_item& itm) : item(itm)
	{
		addAttribute("price", &_price);
		addAttribute("name", &name);
		addFunction("getName", std::function<std::string()>(std::bind(&orderU::item::getName, this)));
	}

};

class ported_order : public http_server::model, public orderU::order
{
public:
	ported_order(orderU::order& t) : order(t)
	{
		addAttribute("item", http_server::model_converter<ported_item, orderU::item>::convert_model(_item));
		addAttribute("subitems", http_server::model_converter< ported_item, orderU::item>::convert_vector(subitems));
		addAttribute("waiter", http_server::model_converter<ported_waiter, orderU::waiter>::convert_model(_waiter));
	}

	ported_order(ported_order& itm) : order(itm)
	{
		addAttribute("item", http_server::model_converter<ported_item, orderU::item>::convert_model(_item));
		addAttribute("subitems", http_server::model_converter< ported_item, orderU::item>::convert_vector(subitems));
		addAttribute("waiter", http_server::model_converter<ported_waiter, orderU::waiter>::convert_model(_waiter));
	}

};

class ported_bill : public http_server::model, public orderU::bill
{
public:
	ported_bill(orderU::bill& o) : bill(o)
	{
		addAttribute("time", &time);
		addAttribute("table", &table);
		addAttribute("orders", http_server::model_converter< ported_order, orderU::order>::convert_vector(orders));
	}

	ported_bill(ported_bill& itm) : bill(itm)
	{
		addAttribute("time", &time);
		addAttribute("table", &table);
		addAttribute("orders", http_server::model_converter< ported_order, orderU::order>::convert_vector(orders));
	}

};

class ported_table : public http_server::model, public orderU::table
{
public:
	ported_table(orderU::table& t) : table(t)
	{
		addAttribute("number", &number);
		addAttribute("orders", http_server::model_converter< ported_order, orderU::order>::convert_vector(orders));
	}

	ported_table(ported_table& itm) : table(itm)
	{
		addAttribute("number", &number);
		addAttribute("orders", http_server::model_converter< ported_order, orderU::order>::convert_vector(orders));

	}

};

class item_controller_impl : http_server::controller 
{
protected:
	std::shared_ptr<orderU::orderu_server> _server;
	
public:
	item_controller_impl(std::shared_ptr<orderU::orderu_server> &server) : controller() , _server(server)
	{
		login_->setLoginPage("/login", "login", "/index");
		for (auto& w : _server->getWaiterManager()->get_list())
		{
			http_server::user_repository::getInstance()->add(std::dynamic_pointer_cast<http_server::User>(http_server::model_converter<ported_waiter, orderU::waiter>::convert_model(w)));
		}

		addCallback({ "/", "/index" }, [&](http_server::model* model)->std::string{
			model->addAttribute("items", http_server::model_converter< ported_item, orderU::item>::convert_vector(_server->getItemManager()->get_list()));
			model->addAttribute("bills", http_server::model_converter< ported_bill, orderU::bill>::convert_vector(_server->getBillManager()->get_list()));
			model->addAttribute("waiters", http_server::model_converter< ported_waiter, orderU::waiter>::convert_vector(_server->getWaiterManager()->get_list()));
			model->addAttribute("tables", http_server::model_converter< ported_table, orderU::table>::convert_vector(_server->getTableManager()->get_list()));
			return "overview";
			}, HTTP_GET, true);

		addCallback({ "/404" }, [&](http_server::model* model)->std::string {
			return "404";
			}, HTTP_GET, true);

		addCallback({ "/items" }, [&](http_server::model* model)->std::string {
			model->addAttribute("items", http_server::model_converter< ported_item, orderU::item>::convert_vector(_server->getItemManager()->get_list()));
			return "items";
			}, HTTP_GET, true);

		addCallback({ "/bills" }, [&](http_server::model* model)->std::string {

			model->addAttribute("bills", http_server::model_converter< ported_bill, orderU::bill>::convert_vector(_server->getBillManager()->get_list()));
			return "bills";
			}, HTTP_GET, true);

		addCallback({ "/waiters" }, [&](http_server::model* model)->std::string {
			model->addAttribute("waiters", http_server::model_converter< ported_waiter, orderU::waiter>::convert_vector(_server->getWaiterManager()->get_list()));
			return "waiters";
			}, HTTP_GET, true);

		addCallback({ "/tables" }, [&](http_server::model* model)->std::string {
			model->addAttribute("tables", http_server::model_converter< ported_table, orderU::table>::convert_vector(_server->getTableManager()->get_list()));
			return "tables";
			}, HTTP_GET, true);
	}

};
#endif