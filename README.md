# OrderU - Server
> Server part of the Project OrderU

[![Boost Version][boost-image]][boost-url]
[![Build Status][travis-image]][travis-url]

This is the server part of "OrderU" a Server-Client for managing Orders. 
The server part manages incomming connections from clients.

![](doc/header.png)

## Installation

OS X & Linux:

```sh
sudo apt install build-essential libboost-1.70-all-dev cmake git
git clone https://gitlab.com/till.brand95/orderu-server
cd orderu-server
mkdir build
cd build
cmake ..
make
```


Windows:

Cmake
Boost
Compiler of choice

## Usage example

A few motivating and useful examples of how your product can be used. Spice this up with code blocks and potentially more screenshots.

_For more examples and usage, please refer to the [Wiki][wiki]._

## Release History

* 0.0.1
    * Work in progress

## Meta

Till Brand – till.brand95@gmail.com
