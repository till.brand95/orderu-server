
#define BOOST_TEST_MODULE OrderU-Server-Test
#include <boost/test/unit_test.hpp>

#include "server.h"
#include "client.h"

using namespace orderU;

BOOST_AUTO_TEST_CASE(server_answer_test)
{

	boost::asio::io_context io_context;
	orderU::server server(io_context, 1000);
	io_context.run();
	BOOST_CHECK(!io_context.stopped());
	io_context.stop();

	BOOST_CHECK(io_context.stopped());
	
}

BOOST_AUTO_TEST_CASE(client_answer_test)
{
	boost::asio::io_context io_context2;
	orderU::server server(io_context2, 1000);
	io_context2.run();


	boost::asio::io_context io_context;
	std::string service("1000");
	std::string host("192.169.0.2");
	client client(io_context, host, service);
	std::thread t([&io_context]() {io_context.run(); });
	message msg;
	msg.id = CMSG_LOGIN;
	msg.data = "1234";
	client.write(msg, [](const message& answer) {
		BOOST_CHECK(answer.id == SMSG_LOGIN_SUCCESS);
		});

	message msg2;
	msg2.id = CMSG_QUERY_TABLE_LIST;
	msg2.data = "";
	client.write(msg2, [](const message& answer) {
		BOOST_CHECK(answer.id == SMSG_TABLE_LIST);
		});
}